Steps to run:

1. Ensure python is installed and updated (just to be sure)
2. Ensure chromium is installed and updated to latest version
3. Open a terminal (or PowerShell, if you're on windows), and navigate to folder containing script
4. Run the command `pip install --upgrade pip`
5. (Optional) Set up a virtual python environment (for example with `python -m venv env`) and activate it
6. Run the command `pip install -r requirements.txt`
7. Run the crawler with `python crawler.py`.

You can run `python crawler.py --help` to be shown a list of arguments and how to use them.
