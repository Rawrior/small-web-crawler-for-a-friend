import re
import os
import time
import random
import logging
import argparse
import pyderman as driver
from selenium import webdriver
from urllib.parse import urljoin
from bs4 import BeautifulSoup as bs

parser = argparse.ArgumentParser(
    prog="Neopian times web crawler/scraper",
    description="A small (and dirty) web crawler to scrape the neopian times, issues 150-980 at most. Further archives can coceivably be fetched with small modifications of this program"
)

def issue_type(issue):
    issue = int(issue)
    if issue < 150:
        raise argparse.ArgumentTypeError("Minimum issue number is 150")
    if issue > 980:
        raise argparse.ArgumentTypeError("Maximum issue number is 980")
    return issue

parser.add_argument('--start-issue', default=150, type=issue_type,
    help="The issue to begin scraping from, inclusive.")
parser.add_argument('--end-issue', default=980, type=issue_type,
    help="The issue to stop scraping at, inclusive")
parser.add_argument('--min-sleep-time', default=1.5, type=float, 
    help="Minimum sleep time after fetching each page. Should probably not be below 1.5 seconds. Time is chosen uniformly between minimum and maximum, so the average will be half of the difference between them.")
parser.add_argument('--max-sleep-time', default=3, type=float, 
    help="Maximum sleep time after fetching each page. Time is chosen uniformly between minimum and maximum, so the average will be half of the difference between them.")
parser.add_argument('--output-path', default="scraped_content", type=str, 
    help="The output path/folder to write content files to. Be aware of your OS path separator (backslash for Windows, forward slash for unix)")
parser.add_argument('--log-level', default="warning", type=str,
    help="Logging level, debug/info/warning/critical/error. Selenium also uses logging and does a lot at debug level, so lowest used in this script is info, and more important stuff is sent with warning.")
parser.add_argument('-v', '--verbose', default=False, action="store_true", 
    help="Sets logging level to INFO, no matter what logging level is previously set")
parser.add_argument('--one-file', default=False, action="store_true",
    help="Toggle whether output should be written to a single file.")

# Little script I made for a friend to scrape a very specific archive
# Luckily, the archive is pretty well-formatted and consistent, so getting each site is pretty
# easy, and I know what to expect from each site. I also know I only need to go 3 layers deep,
# as the structure goes archive -> content for the week -> full stories
# Thus, I just need to get links from the archive, then get links inside each of those links
# and then scrape the actual content from the final 'layer'

# Generally, this code is kinda ugly, but it's also very domain-specific, so I don't mind too much
# a general-purpose web crawler would and should not be built this way.

def rand_sleep(min, max):
    choice = random.uniform(min, max)
    logging.info(f"Sleeping for {choice:.2f} sec to make sure the site loads first")
    time.sleep(choice)


def start_selenium_browser():
    # Make sure our preferred webdriver is installed. (Chrome, for no reason)
    path = driver.install(browser=driver.chrome)
    logging.warning(f"Chromedriver is installed to path: {path}")

    # Headless, since we only care about the links and content on the pages
    options = webdriver.ChromeOptions()
    options.add_argument("--headless")

    # Start the browser session itself
    return webdriver.Chrome(options=options)


if __name__ == "__main__":
    args = parser.parse_args()
    if args.start_issue > args.end_issue:
        print("Start issue must be less than or equal to end issue!")
        exit()

    # Set logging level
    numeric_level = getattr(logging, args.log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)

    if args.verbose:
        numeric_level = getattr(logging, "INFO", None)
        
    logging.basicConfig(level=numeric_level)


    sleep_amount_min = args.min_sleep_time
    sleep_amount_max = args.max_sleep_time
    start_issue = args.start_issue
    # +1, since range end is non-inclusive
    end_issue = args.end_issue + 1
    output_path = args.output_path

    # Beginning URL we want to scrape from
    base_url = "https://www.neopets.com/ntimes/index.phtml?section=archives"
    # Initial next links we want to follow
    url_match = "ntimes/index.phtml\?week="

    # Holding variables to generate filenames later
    week = ""
    section = ""
    article_title = ""
    article_id = ""

    # Get a browser session
    browser = start_selenium_browser()
    times = []  # To hold fetch times per issue later

    # We know that the initial links to follow are always structured such that they include the
    # week (AKA issue), and the section (AKA articles, short stories, etc.), and that the order
    # of these in the URL does not matter. So, instead of fetching the archive list, then fetching
    # an issue, then fetching the section links, we can just generate them on our own and save
    # a little bit of time.

    # Range uses non-inclusive upper bound
    for week_num in [x for x in range(start_issue, end_issue)]:
        start_time = time.time()

        week = str(week_num)
        for section_addon in ["articles", "shorts", "series", "cont"]:
            logging.info(f"Generating urls for section: {section_addon}")
            # Build the URL
            section_url = f'https://www.neopets.com/ntimes/index.phtml?week={week_num}&section={section_addon}'

            section = section_addon
            # Fetch, then sleep to make sure the page loads before we handle it
            browser.get(section_url)
            rand_sleep(sleep_amount_min, sleep_amount_max)

            # Extract the table with the actual content (Neopets always does it like this, which
            # makes it pretty easy to find!)
            html = browser.page_source
            soup = bs(html, features="html.parser")
            table = soup.find(name="td", attrs={'class': 'content'})

            # Regex explanation: Negative lookbehind assertion, since we *only* want links to
            # articles within the newest archive (this problem came up 1 time, so might as well)
            # handle it.
            # Then, match string directly, and look for an href with a number after a section param
            table_urls = table.find_all("a", href=re.compile("(?<!newnt/)index.phtml\?section=\d"))

            third_layer_urls = []
            # For each link found, get the final link with urljoin
            for turl in table_urls:
                full_url = urljoin(section_url, turl.attrs.get("href"))
                third_layer_urls.append(full_url)

            # Remove duplicates (set), but keep as a list
            third_layer_urls = list(set(third_layer_urls))
            logging.info(f"Printing all third layer URLs for section {section}:")
            logging.info(third_layer_urls)

            # For each link we found...
            for url in third_layer_urls:
                # Get the article ID from the url (in a kinda dirty way, tbh)
                section_search = re.compile("section=([^&]*)")
                article_id = re.search(section_search, url).group(1)

                # Fetch, then sleep to make sure the site is loaded
                browser.get(url)
                rand_sleep(sleep_amount_min, sleep_amount_max)

                # Get to the actual content
                html = browser.page_source
                soup = bs(html, features="html.parser")

                # The title is gonna be the first (and probably only) thing using h1, so extract,
                # then replace non-alphanumeric characters with underscore, and truncate longer
                # sequences of underscores to just 1
                article_title = soup.find("h1").text.strip()
                article_title = re.sub(r'[^\w]', '_', article_title)
                article_title = re.sub(r'_{2,10}', '_', article_title)

                # Find the table element with the actual article
                table = soup.find(name="td", attrs={'class': 'content'})
                # Get just the text. Ignores weird character codes, images, html tags and such. Nice
                table_text = table.text
                # Remove unnecessary whitespace before and after
                trimmed_table_text = table_text.strip()
                logging.info(f"Article title: {article_title}")
                # Generate filename and write to file
                filename = f"{week}-{section}-{article_id}-{article_title}.txt"
                logging.info(f"Generated filename: {filename}")

                # Create output path if it doesn't exist
                if not os.path.exists(output_path):
                    os.makedirs(output_path)
                if args.one_file:
                    with open(f"{output_path}{os.path.sep}full_output.txt", "a") as text_file:
                        text_file.write(trimmed_table_text)
                        text_file.write("\n\n")
                else:
                    with open(f"{output_path}{os.path.sep}{filename}", "w+") as text_file:
                        text_file.write(trimmed_table_text)

        # Performance 'monitoring' stuff
        end_time = time.time()
        logging.warning(f"Elapsed time: {end_time - start_time} seconds")
        times.append(end_time - start_time)
        logging.warning(f"Average time elapsed to fetch an issue so far: {sum(times) / len(times)}")
    
    logging.warning(f"Average time elapsed to fetch each issue: {sum(times) / len(times)}")

    browser.quit()